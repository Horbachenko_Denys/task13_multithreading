package com.epam.model;

import java.time.LocalDateTime;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class MyReadWriteLock implements Runnable {
    private static ReadWriteLock rwLock = new ReentrantReadWriteLock();
    private static Lock readLock = rwLock.readLock();
    private static Lock writeLock = rwLock.writeLock();
    private static long A = 0;

        @Override
        public void run() {
            readLock.lock();
            try {
                System.out.println(LocalDateTime.now() + Thread.currentThread().getName() + "A=" + A);
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                readLock.unlock();
            }
        }

        public void show() {
            ExecutorService executor = Executors.newFixedThreadPool(4);
            Runnable rT1 = new Thread();
            Runnable rT2 = new Thread();
            Runnable rT3 = new Thread();
            executor.submit(rT1);
            executor.submit(rT2);
            executor.submit(rT3);
            executor.submit(() ->{ writeLock.lock();
            try {
                A = 25;
                System.out.println(LocalDateTime.now() + Thread.currentThread().getName() + " A=" + A);
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                writeLock.unlock();
            }});
            executor.submit(rT1);
            executor.submit(rT2);
            executor.submit(rT3);
        }
    }
