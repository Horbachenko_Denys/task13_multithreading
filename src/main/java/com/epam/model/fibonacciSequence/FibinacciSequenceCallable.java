package com.epam.model.fibonacciSequence;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class FibinacciSequenceCallable {

    private int i = 0;
    private int b = 0;
    private int c = 1;

    ExecutorService executorService = Executors.newSingleThreadExecutor();
    public Future<String> future = executorService.submit(() -> {
        String fibonacciSequence = "Fibonacci series: ";
        for (int j = 1; j <= i; j++) {
            int a = b;
            b = c;
            c = a + b;
            fibonacciSequence = fibonacciSequence.concat(Integer.toString(a).concat(" "));
        }
        String response = String.format("Fibonacci with input parameter %d finished", i);
        System.out.println(response);
        return fibonacciSequence;
    });

    public FibinacciSequenceCallable(int i) {
        this.i = i;
    }
}
