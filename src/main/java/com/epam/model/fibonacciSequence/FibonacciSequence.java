package com.epam.model.fibonacciSequence;

public class FibonacciSequence extends Thread {

    //    private Thread t;
    private final int i;
    private int b = 0;
    private int c = 1;

    public FibonacciSequence(int i) {
        this.i = i;
    }

    @Override
    public void run() {
        String fibonacciSequence = "Fibonacci series: ";
        try {
            System.out.println("Start fibonacci execution");
            Thread.sleep(2000);
            for (int j = 1; j <= i; j++) {
                int a = b;
                b = c;
                c = a + b;
                fibonacciSequence = fibonacciSequence.concat(Integer.toString(a).concat(" "));
            }
            System.out.println(fibonacciSequence);

        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
