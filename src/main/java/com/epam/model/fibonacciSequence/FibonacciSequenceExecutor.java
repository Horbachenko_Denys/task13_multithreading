package com.epam.model.fibonacciSequence;

public class FibonacciSequenceExecutor implements Runnable {

    private  int i;
    private int b = 0;
    private int c = 1;

    public FibonacciSequenceExecutor(int i) {
        this.i = i;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.print("Fibonacci series: ");
        for (int j = 1; j <= i; j++) {
            int a = b;
            b = c;
            c = a + b;
            System.out.print(a + " ");
        }
        System.out.println();
    }
}
