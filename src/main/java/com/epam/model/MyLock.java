package com.epam.model;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class MyLock implements Runnable {

    Lock l = new ReentrantLock();

    public void startMyLock() {
        Thread t1 = new Thread(this);
        Thread t2 = new Thread(this);
        Thread t3 = new Thread(this);
        t1.start();
        t2.start();
        t3.start();
        try {
            t1.join();
            t2.join();
            t3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("End");

    }

    private void method1() {
        if (l.tryLock()) {
            try {
                System.out.println("Inside method 1");
            } finally {
                l.unlock();
            }
        }
    }

    private void method2() {
        if (l.tryLock()) {
            try {
                System.out.println("Inside method 2");
            } finally {
                l.unlock();
            }
        }
    }

    private void method3() {
        if (l.tryLock()) {
            try {
                System.out.println("Inside method 3");
            } finally {
                l.unlock();
            }
        }
    }

    @Override
    public void run() {
        this.method1();
        this.method2();
        this.method3();
        System.out.println("finish" + Thread.currentThread().getName());
    }
}
