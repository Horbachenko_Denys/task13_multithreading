package com.epam.model;

import com.epam.model.fibonacciSequence.FibinacciSequenceCallable;
import com.epam.model.fibonacciSequence.FibonacciSequence;
import com.epam.model.fibonacciSequence.FibonacciSequenceExecutor;
import com.epam.model.pipe.MyPipe;

import java.util.Random;
import java.util.concurrent.*;

public class ModelImpl implements Model {

    FibinacciSequenceCallable fibinacciSequenceCallable;
    SyncClass syncClass;
    private volatile long A = 0;
    private static Object sync;

    @Override
    public void startPingPong()  {
        sync = new Object();
        Thread t1 = new Thread(() -> {synchronized (sync) {
        for (int i = 1; i <= 10000; i++) {
        try {sync.wait();} catch (InterruptedException e) {
            e.printStackTrace();
        }
        A++;
            System.out.println(Thread.currentThread().getName() + " " + A);
        sync.notify();
        }
            System.out.println("finish" + Thread.currentThread().getName());
        }});
        Thread t2 = new Thread(() -> {synchronized (sync){
        for (int i = 1; i <= 10000; i++) {
            sync.notify();
            try { sync.wait(); } catch (InterruptedException e) {
                e.printStackTrace();
            }
            A++;
            System.out.println(Thread.currentThread().getName() + " " + A);
        }
            System.out.println("finish" + Thread.currentThread().getName());
        }});
        t1.start();
        t2.start();
        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("A = " + A);
    }

    @Override
    public void createFibonacci() {
        for (int i = 5; i < 10; i++) {
           FibonacciSequence fibonacciSequence = new FibonacciSequence(i);
            fibonacciSequence.start();
//            try {
//                fibonacciSequence.join();
//            } catch (InterruptedException e) {
//                throw new RuntimeException(e);
//            }
        }
    }

    @Override
    public void createFibonacciWithExecutors() {
        for (int i = 5; i <= 10; i++) {
            Executor executor = Executors.newSingleThreadExecutor();
            executor.execute(new FibonacciSequenceExecutor(i));
        }
    }

    @Override
    public void createFibonacciWithCallable() {
        for (int i = 5; i <= 10; i++) {
            fibinacciSequenceCallable = new FibinacciSequenceCallable(i);
            try {
                Future<String> futureFibonacci = fibinacciSequenceCallable.future;
                Thread.sleep(2000);
                System.out.println(futureFibonacci.get());
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void createSleeper(int i) {
        ExecutorService service = Executors.newScheduledThreadPool(3);
        for (int j = 0; j < i; j++) {
        service.submit(() -> {
            Random random = new Random();
            int n = (random.nextInt(10))*1000;
            try {
                Thread.sleep(n);
                System.out.println("Sleep about " + n / 1000 + " seconds");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });}
        service.shutdown();
    }

    @Override
    public void createSynchronizedMethods() {
//        syncClass = new SyncClass();
//        syncClass.startClass();
        MyLock myLock = new MyLock();
        myLock.startMyLock();
    }

    @Override
    public void createCommunicatedPipes() {
        MyPipe myPipe = new MyPipe();

    }

    @Override
    public void createReadWriteLock() {
        MyReadWriteLock readWriteLock = new MyReadWriteLock();
        readWriteLock.show();
    }
}